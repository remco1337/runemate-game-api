package com.runemate.game.api.hybrid.cache.configs;

import static com.runemate.game.api.hybrid.cache.db.DBTableIndex.*;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.io.*;
import lombok.*;

@EqualsAndHashCode(callSuper = false)
@Data
@CustomLog
public class DBRow extends IncrementallyDecodedItem {

    private final int id;
    private int tableId;
    private CS2VarType[][] columnTypes;
    private Object[][] columnValues;

    @Override
    protected void decode(final Js5InputStream stream, final int opcode) throws IOException {
        switch (opcode) {
            case 3:
                int numColumns = stream.readUnsignedByte();
                CS2VarType[][] types = new CS2VarType[numColumns][];
                Object[][] columnValues = new Object[numColumns][];

                for (int columnId = stream.readUnsignedByte(); columnId != 255; columnId = stream.readUnsignedByte()) {
                    CS2VarType[] columnTypes = new CS2VarType[stream.readUnsignedByte()];
                    for (int i = 0; i < columnTypes.length; i++) {
                        columnTypes[i] = CS2VarType.fromId(stream.readShortSmart());
                    }
                    types[columnId] = columnTypes;
                    columnValues[columnId] = decodeColumnFields(stream, columnTypes);
                }

                setColumnTypes(types);
                setColumnValues(columnValues);
                break;
            case 4:
                setTableId(stream.readVarInt2());
                break;
            default:
                log.warn("Unrecognized dbrow opcode " + opcode);
                break;
        }
    }
}
