package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class DBRowLoader extends SerializedFileLoader<DBRow> {

    public DBRowLoader() {
        super(CacheIndex.CONFIGS.getId());
    }

    @Override
    protected DBRow construct(final int entry, final int file, final Map<String, Object> arguments) {
        return new DBRow(file);
    }
}
