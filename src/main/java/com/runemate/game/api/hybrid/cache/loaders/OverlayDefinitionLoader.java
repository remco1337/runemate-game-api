package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.io.*;
import java.util.*;

public final class OverlayDefinitionLoader extends SerializedFileLoader<CacheOverlayDefinition> {
    public OverlayDefinitionLoader() {
        super(CacheIndex.CONFIGS.getId());
    }

    @Override
    protected CacheOverlayDefinition construct(int entry, int file, Map<String, Object> arguments) {
        return new CacheOverlayDefinition(file);
    }

    public CacheOverlayDefinition load(int id) {
        try {
            return load(ConfigType.OVERLAY, id);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.err.println("Failed to load overlay definition " + id);
        }
        return null;
    }
}
