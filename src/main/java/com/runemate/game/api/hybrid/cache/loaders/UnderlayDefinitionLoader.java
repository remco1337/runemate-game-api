package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class UnderlayDefinitionLoader extends SerializedFileLoader<CacheUnderlayDefinition> {
    public UnderlayDefinitionLoader() {
        super(Js5Cache.RS3Archives.JS5_CONFIG);
    }

    @Override
    protected CacheUnderlayDefinition construct(int entry, int file, Map<String, Object> arguments) {
        return new CacheUnderlayDefinition(file);
    }
}
