package com.runemate.game.api.hybrid.local.hud.interfaces;

/**
 * @deprecated Replaced with InputDialog to accommodate String inputs
 * <p>
 * Hybrid helper class for the "Enter amount:" dialog seen in banks, deposit boxes and elsewhere
 **/
@Deprecated
public class EnterAmountDialog {

    private final static int OSRS_WIDGET = 162;

    private EnterAmountDialog() {
    }

    /**
     * Determines whether or not the "Enter amount:" dialog is open
     *
     * @return true if the dialog is open
     */
    public static boolean isOpen() {
        return InputDialog.isOpen();
    }

    /**
     * Enters the amount specified into the dialog box and delays until the action was successful
     *
     * @param amount amount to type
     * @return if successfully typed the amount, selected enter and confirmed the dialog has closed
     */
    public static boolean enterAmount(final int amount) {
        return InputDialog.enterAmount(amount);
    }

    /**
     * @param amount amount to type
     * @param metric true to force truncation of amount, false to type whole number
     * @return if successfully typed the amount, selected enter and confirmed the dialog has closed
     */
    public static boolean enterAmount(final int amount, final boolean metric) {
        return InputDialog.enterAmount(amount, metric);
    }

    /**
     * @param text string to type
     * @return if successfully typed the amount, selected enter and confirmed the dialog has closed
     */
    public static boolean enterAmount(final String text) {
        return InputDialog.enterText(text);
    }

    private static InterfaceComponent getTextEntryComponent() {
        return Interfaces.newQuery().containers(OSRS_WIDGET)
            .textContains(
                "Enter amount:",
                "How many do you wish to buy?",
                "How many do you wish to sell?",
                "Set a price for each item:",
                "Enter amount (",
                "How many coins do you wish to stake?"
            )
            .grandchildren(false).types(InterfaceComponent.Type.LABEL)
            .results().first();
    }
}
