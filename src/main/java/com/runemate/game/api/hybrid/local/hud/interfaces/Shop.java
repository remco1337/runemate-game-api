package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import javax.annotation.*;

//TODO maybe deprecate... it's a pain to maintain, especially on RS3
public final class Shop {

    private Shop() {

    }

    public static String getName() {
        return OSRSShop.getName();
    }

    public static boolean isOpen() {
        return OSRSShop.isOpen();
    }

    /**
     * Closes the Shop
     *
     * @return true if the shop is closed
     */
    public static boolean close() {
        return OSRSShop.close();
    }

    /**
     * @param filter   the filter to use to find the item to buy
     * @param quantity the quantity of the item to buy
     * @return true if the inventory's amount of items increases by the given quantity
     */
    public static boolean buy(final Predicate<SpriteItem> filter, final int quantity) {
        return OSRSShop.buy(filter, quantity);
    }

    public static boolean buy(final int id, final int quantity) {
        return buy(spriteItem -> id == spriteItem.getId(), quantity);
    }

    public static boolean buy(final String name, final int quantity) {
        return buy(Items.getNamePredicate(name), quantity);
    }

    /**
     * Sells a certain amount of an item to the shop
     *
     * @param filter   the filter to use to find the item to sell
     * @param quantity the quantity to sell
     * @return true if the amount of the given item decreased by the given quantity
     */
    public static boolean sell(final Predicate<SpriteItem> filter, final int quantity) {
        return OSRSShop.sell(filter, quantity);
    }

    public static boolean sell(final int id, final int quantity) {
        return sell(spriteItem -> id == spriteItem.getId(), quantity);
    }

    public static boolean sell(final String name, final int quantity) {
        return sell(Items.getNamePredicate(name), quantity);
    }

    /**
     * Checks if any items match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if an item matches the filter
     */
    public static boolean contains(Predicate<SpriteItem> filter) {
        return Items.contains(getItems(), filter);
    }

    /**
     * Checks if any items match the given id
     *
     * @param id the id to check the items against
     * @return true if an item matches the id
     */
    public static boolean contains(int id) {
        return Items.contains(getItems(), id);
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(String name) {
        return Items.contains(getItems(), name);
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(Pattern name) {
        return Items.contains(getItems(), name);
    }

    /**
     * Checks if the supplied {@link Predicate filter} matches at least one item
     *
     * @param predicate the predicate to check the items against
     * @return true if the predicate matches an item
     */
    public static boolean containsAllOf(final Predicate<SpriteItem> predicate) {
        return Items.containsAllOf(getItems(), predicate);
    }

    /**
     * Checks if all of the supplied {@link Predicate filter}s match at least one item each
     *
     * @param filters the predicates to check the items against
     * @return true if all of the predicates have a match
     */
    @SafeVarargs
    public static boolean containsAllOf(final Predicate<SpriteItem>... filters) {
        return Items.containsAllOf(getItems(), filters);
    }

    /**
     * Checks if all of the supplied ids match at least one item each
     *
     * @param ids the ids to check the items against
     * @return true if all of the ids have a match
     */
    public static boolean containsAllOf(final int... ids) {
        return Items.containsAllOf(getItems(), ids);
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final String... names) {
        return Items.containsAllOf(getItems(), names);
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final Pattern... names) {
        return Items.containsAllOf(getItems(), names);
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if at least one item doesn't match the filter
     */
    public static boolean containsAnyExcept(final Predicate<SpriteItem> filter) {
        return Items.containsAnyExcept(getItems(), filter);
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}s
     *
     * @param filters the predicates to check the items against
     * @return true if at least one item doesn't match the filters
     */
    @SafeVarargs
    public static boolean containsAnyExcept(final Predicate<SpriteItem>... filters) {
        return Items.containsAnyExcept(getItems(), filters);
    }

    /**
     * Checks if any items don't match the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item doesn't match the ids
     */
    public static boolean containsAnyExcept(final int... ids) {
        return Items.containsAnyExcept(getItems(), ids);
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final String... names) {
        return Items.containsAnyExcept(getItems(), names);
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final Pattern... names) {
        return Items.containsAnyExcept(getItems(), names);
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if at least one item matches the filter
     */
    public static boolean containsAnyOf(final Predicate<SpriteItem> filter) {
        return Items.containsAnyOf(getItems(), filter);
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if at least one item matches a filter
     */
    @SafeVarargs
    public static boolean containsAnyOf(final Predicate<SpriteItem>... filters) {
        return Items.containsAnyOf(getItems(), filters);
    }

    /**
     * Checks if any item matches the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item matches an id
     */
    public static boolean containsAnyOf(final int... ids) {
        return Items.containsAnyOf(getItems(), ids);
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final String... names) {
        return Items.containsAnyOf(getItems(), names);
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final Pattern... names) {
        return Items.containsAnyOf(getItems(), names);
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if all items match the filter
     */
    public static boolean containsOnly(final Predicate<SpriteItem> filter) {
        return Items.containsOnly(getItems(), filter);
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if all items match at least one filter each
     */
    @SafeVarargs
    public static boolean containsOnly(final Predicate<SpriteItem>... filters) {
        return Items.containsOnly(getItems(), filters);
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final String... names) {
        return Items.containsOnly(getItems(), names);
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final Pattern... names) {
        return Items.containsOnly(getItems(), names);
    }

    /**
     * Gets the total quantity of items
     *
     * @return the total quantity of items
     */
    public static int getQuantity() {
        return Items.getQuantity(getItems());
    }

    /**
     * Gets the total quantity of items matching the filter
     *
     * @param filter the filter to check the items against
     * @return the total quantity of items matching the filter
     */
    public static int getQuantity(final Predicate<SpriteItem> filter) {
        return Items.getQuantity(getItems(), filter);
    }

    /**
     * Gets the total quantity of items matching the {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return the total quantity of items matching the filters
     */
    @SafeVarargs
    public static int getQuantity(final Predicate<SpriteItem>... filters) {
        return Items.getQuantity(getItems(), filters);
    }

    /**
     * Gets the total quantity of items matching the ids
     *
     * @param ids the ids to check the items against
     * @return the total quantity of items matching the ids
     */
    public static int getQuantity(final int... ids) {
        return Items.getQuantity(getItems(), ids);
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final String... names) {
        return Items.getQuantity(getItems(), names);
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final Pattern... names) {
        return Items.getQuantity(getItems(), names);
    }


    /**
     * Gets the bounds of the given index
     *
     * @param index the index to get the bounds of
     * @return the bounds of the index as an InteractableRectangle
     */
    @Nullable
    public static InteractableRectangle getBoundsOf(final int index) {
        final List<InteractableRectangle> bounds = getSlotBounds();
        return index >= 0 && index < bounds.size() ? bounds.get(index) : null;
    }

    /**
     * Gets the SpriteItem in the given index
     *
     * @param index the index to get the SpriteItem from
     * @return The SpriteItem in the given index
     */
    @Nullable
    public static SpriteItem getItemIn(final int index) {
        return newQuery().indices(index).results().first();
    }

    public static SpriteItemQueryResults getItems() {
        return getItems((Predicate<SpriteItem>) null);
    }

    @Nonnull
    public static SpriteItemQueryResults getItems(final Predicate<SpriteItem> filter) {
        return OSRSShop.getItems(filter);
    }

    @Nonnull
    public static SpriteItemQueryResults getItems(final int... ids) {
        return getItems(spriteItem -> Arrays.stream(ids).anyMatch(id -> spriteItem.getId() == id));
    }

    @Nonnull
    public static SpriteItemQueryResults getItems(final String... names) {
        return getItems(Items.getNamePredicate(names));
    }

    @Nonnull
    public static SpriteItemQueryResults getItems(final Pattern... names) {
        return getItems(Items.getNamePredicate(names));
    }

    public static List<InteractableRectangle> getSlotBounds() {
        return OSRSShop.getSlotBounds();
    }

    @Nullable
    public static InteractableRectangle getViewport() {
        return null;
    }

    public static SpriteItemQueryBuilder newQuery() {
        return new SpriteItemQueryBuilder(null).provider(
            () -> getItems((Predicate<SpriteItem>) null).asList());
    }

    @RS3Only
    @Deprecated
    public final static class Free {
        public static SpriteItemQueryResults getItems(Predicate<SpriteItem> filter) {
            return new SpriteItemQueryResults();
        }

        public static SpriteItemQueryResults getItems() {
            return getItems((Predicate<SpriteItem>) null);
        }

        public static SpriteItemQueryResults getItems(final int... ids) {
            return getItems(Items.getIdPredicate(ids));
        }

        public static SpriteItemQueryResults getItems(final String... names) {
            return getItems(Items.getNamePredicate(names));
        }

        public static SpriteItemQueryResults getItems(final Pattern... names) {
            return getItems(Items.getNamePredicate(names));
        }

        /**
         * Checks if any items match the given {@link Predicate filter}
         *
         * @param filter the predicate to check the items against
         * @return true if an item matches the filter
         */
        public static boolean contains(Predicate<SpriteItem> filter) {
            return Items.contains(getItems(), filter);
        }

        /**
         * Checks if any items match the given id
         *
         * @param id the id to check the items against
         * @return true if an item matches the id
         */
        public static boolean contains(int id) {
            return Items.contains(getItems(), id);
        }

        /**
         * Checks if any items match the given name
         *
         * @param name the name to check the items against
         * @return true if an item matches the name
         */
        public static boolean contains(String name) {
            return Items.contains(getItems(), name);
        }

        /**
         * Checks if any items match the given name
         *
         * @param name the name to check the items against
         * @return true if an item matches the name
         */
        public static boolean contains(Pattern name) {
            return Items.contains(getItems(), name);
        }

        /**
         * Checks if the supplied {@link Predicate filter} matches at least one item
         *
         * @param predicate the predicate to check the items against
         * @return true if the predicate matches an item
         */
        public static boolean containsAllOf(final Predicate<SpriteItem> predicate) {
            return Items.containsAllOf(getItems(), predicate);
        }

        /**
         * Checks if all of the supplied {@link Predicate filter}s match at least one item each
         *
         * @param filters the predicates to check the items against
         * @return true if all of the predicates have a match
         */
        @SafeVarargs
        public static boolean containsAllOf(final Predicate<SpriteItem>... filters) {
            return Items.containsAllOf(getItems(), filters);
        }

        /**
         * Checks if all of the supplied ids match at least one item each
         *
         * @param ids the ids to check the items against
         * @return true if all of the ids have a match
         */
        public static boolean containsAllOf(final int... ids) {
            return Items.containsAllOf(getItems(), ids);
        }

        /**
         * Checks if all of the supplied names match at least one item each
         *
         * @param names the names to check the items against
         * @return true if all of the names have a match
         */
        public static boolean containsAllOf(final String... names) {
            return Items.containsAllOf(getItems(), names);
        }

        /**
         * Checks if all of the supplied names match at least one item each
         *
         * @param names the names to check the items against
         * @return true if all of the names have a match
         */
        public static boolean containsAllOf(final Pattern... names) {
            return Items.containsAllOf(getItems(), names);
        }

        /**
         * Checks if any items don't match the given {@link Predicate filter}
         *
         * @param filter the predicate to check the items against
         * @return true if at least one item doesn't match the filter
         */
        public static boolean containsAnyExcept(final Predicate<SpriteItem> filter) {
            return Items.containsAnyExcept(getItems(), filter);
        }

        /**
         * Checks if any items don't match the given {@link Predicate filter}s
         *
         * @param filters the predicates to check the items against
         * @return true if at least one item doesn't match the filters
         */
        @SafeVarargs
        public static boolean containsAnyExcept(final Predicate<SpriteItem>... filters) {
            return Items.containsAnyExcept(getItems(), filters);
        }

        /**
         * Checks if any items don't match the given ids
         *
         * @param ids the ids to check the items against
         * @return true if at least one item doesn't match the ids
         */
        public static boolean containsAnyExcept(final int... ids) {
            return Items.containsAnyExcept(getItems(), ids);
        }

        /**
         * Checks if any items don't match the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item doesn't match the names
         */
        public static boolean containsAnyExcept(final String... names) {
            return Items.containsAnyExcept(getItems(), names);
        }

        /**
         * Checks if any items don't match the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item doesn't match the names
         */
        public static boolean containsAnyExcept(final Pattern... names) {
            return Items.containsAnyExcept(getItems(), names);
        }

        /**
         * Checks if any item matches the given {@link Predicate filter}
         *
         * @param filter the filter to check the items against
         * @return true if at least one item matches the filter
         */
        public static boolean containsAnyOf(final Predicate<SpriteItem> filter) {
            return Items.containsAnyOf(getItems(), filter);
        }

        /**
         * Checks if any item matches the given {@link Predicate filter}s
         *
         * @param filters the filters to check the items against
         * @return true if at least one item matches a filter
         */
        @SafeVarargs
        public static boolean containsAnyOf(final Predicate<SpriteItem>... filters) {
            return Items.containsAnyOf(getItems(), filters);
        }

        /**
         * Checks if any item matches the given ids
         *
         * @param ids the ids to check the items against
         * @return true if at least one item matches an id
         */
        public static boolean containsAnyOf(final int... ids) {
            return Items.containsAnyOf(getItems(), ids);
        }

        /**
         * Checks if any item matches the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item matches a name
         */
        public static boolean containsAnyOf(final String... names) {
            return Items.containsAnyOf(getItems(), names);
        }

        /**
         * Checks if any item matches the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item matches a name
         */
        public static boolean containsAnyOf(final Pattern... names) {
            return Items.containsAnyOf(getItems(), names);
        }

        /**
         * Checks if all of the items match the given {@link Predicate filter}
         *
         * @param filter the filter to check the items against
         * @return true if all items match the filter
         */
        public static boolean containsOnly(final Predicate<SpriteItem> filter) {
            return Items.containsOnly(getItems(), filter);
        }

        /**
         * Checks if all of the items match the given {@link Predicate filter}s
         *
         * @param filters the filters to check the items against
         * @return true if all items match at least one filter each
         */
        @SafeVarargs
        public static boolean containsOnly(final Predicate<SpriteItem>... filters) {
            return Items.containsOnly(getItems(), filters);
        }

        /**
         * Checks if all of the items match the given names
         *
         * @param names the filters to check the items against
         * @return true if all items match at least one name each
         */
        public static boolean containsOnly(final String... names) {
            return Items.containsOnly(getItems(), names);
        }

        /**
         * Checks if all of the items match the given names
         *
         * @param names the filters to check the items against
         * @return true if all items match at least one name each
         */
        public static boolean containsOnly(final Pattern... names) {
            return Items.containsOnly(getItems(), names);
        }

        /**
         * Gets the total quantity of items
         *
         * @return the total quantity of items
         */
        public static int getQuantity() {
            return Items.getQuantity(getItems());
        }

        /**
         * Gets the total quantity of items matching the filter
         *
         * @param filter the filter to check the items against
         * @return the total quantity of items matching the filter
         */
        public static int getQuantity(final Predicate<SpriteItem> filter) {
            return Items.getQuantity(getItems(), filter);
        }

        /**
         * Gets the total quantity of items matching the {@link Predicate filter}s
         *
         * @param filters the filters to check the items against
         * @return the total quantity of items matching the filters
         */
        @SafeVarargs
        public static int getQuantity(final Predicate<SpriteItem>... filters) {
            return Items.getQuantity(getItems(), filters);
        }

        /**
         * Gets the total quantity of items matching the ids
         *
         * @param ids the ids to check the items against
         * @return the total quantity of items matching the ids
         */
        public static int getQuantity(final int... ids) {
            return Items.getQuantity(getItems(), ids);
        }

        /**
         * Gets the total quantity of items matching the names
         *
         * @param names the ids to check the items against
         * @return the total quantity of items matching the names
         */
        public static int getQuantity(final String... names) {
            return Items.getQuantity(getItems(), names);
        }

        /**
         * Gets the total quantity of items matching the names
         *
         * @param names the ids to check the items against
         * @return the total quantity of items matching the names
         */
        public static int getQuantity(final Pattern... names) {
            return Items.getQuantity(getItems(), names);
        }

        public static List<InteractableRectangle> getSlotBounds() {
            return Collections.emptyList();
        }

        @Nullable
        public static InteractableRectangle getBoundsOf(int index) {
            List<InteractableRectangle> bounds = Shop.Free.getSlotBounds();
            return bounds.size() < index ? bounds.get(index) : null;
        }

        public static SpriteItemQueryBuilder newQuery() {
            return new SpriteItemQueryBuilder(null).provider(
                () -> getItems((Predicate<SpriteItem>) null).asList());
        }

        public static SpriteItem getItemIn(int index) {
            return newQuery().indices(index).results().first();
        }
    }
}
