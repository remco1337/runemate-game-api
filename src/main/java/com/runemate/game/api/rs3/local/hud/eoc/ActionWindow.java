package com.runemate.game.api.rs3.local.hud.eoc;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.rs3.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import javax.annotation.*;

/**
 * ActionWindows are the windows opened by clicking the icons within the various management windows within the ribbon bar (all official naming)
 */
@Deprecated
public enum ActionWindow implements Openable, Closeable, Lockable {
    BACKPACK,
    WORN_EQUIPMENT,
    SKILLS,
    PRAYERS,
    MAGIC_BOOK,
    @Deprecated
    MAGIC_ABILITIES,
    MELEE_ABILITIES,
    RANGED_ABILITIES,
    DEFENCE_ABILITIES,
    CONSTITUTION_ABILITIES,
    DEFENSIVE_ABILITIES,
    FRIENDS,
    EMOTES,
    FAMILIAR,
    MINIGAMES;

    /**
     * For one click actionwindows (like summoning)
     */
    ActionWindow() {
        // no-op
    }

    /**
     * Gets all open action windows
     */
    @Nonnull
    public static List<ActionWindow> getOpen() {
        return Collections.emptyList();
    }

    private static InterfaceComponent getRibbonWindowButtonBar() {
        return null;
    }

    @Deprecated
    public static boolean isCustomizationLocked() {
        return false;
    }

    public static boolean isLayoutLocked() {
        return false;
    }

    @Deprecated
    public InterfaceComponent getWindowTab() {
        return null;
    }

    public InterfaceComponent getTab() {
        return null;
    }

    public boolean isTabbed() {
        return false;
    }

    private boolean isTabbed(InterfaceComponent tab) {
        return false;
    }

    private boolean scrollToTab(InterfaceComponent tab) {
        return false;
    }

    public boolean isTabDisplayed() {
        return false;
    }

    private boolean isTabDisplayed(InterfaceComponent tab) {
        return false;
    }

    @Override
    public boolean close() {
        return false;
    }

    @Override
    @Deprecated
    public boolean isClosed() {
        return false;
    }

    @Override
    public boolean isLocked() {
        return false;
    }

    /**
     * Opens up the window so it's visible
     */
    @Override
    public boolean open() {
        return false;
    }

    /**
     * Checks if the window is visible
     */
    @Override
    public boolean isOpen() {
        return false;
    }

    @Override
    public String toString() {
        return "ActionWindow." + name();
    }
}
