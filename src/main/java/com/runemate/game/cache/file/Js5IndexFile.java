package com.runemate.game.cache.file;

import com.runemate.game.cache.*;
import com.runemate.game.cache.io.*;
import java.io.*;
import java.nio.file.*;
import java.util.concurrent.*;

public class Js5IndexFile extends Js5File {
    private final int archiveIndex;
    private final JS5CacheController js5_cache;
    private final ReferenceTable referenceTable;
    private ConcurrentMap<Integer, InitialChunkInfo> initial_chunks;

    public Js5IndexFile(
        JS5CacheController js5_cache, int archiveIndex, File file,
        OpenOption... mode
    ) {
        super(file, mode);
        this.archiveIndex = archiveIndex;
        this.js5_cache = js5_cache;
        this.referenceTable = new ReferenceTable(this);
    }

    public ConcurrentMap<Integer, InitialChunkInfo> getInitialChunksInfo() {
        if (initial_chunks == null) {
            try {
                initial_chunks = new ConcurrentHashMap<>();
                synchronized (initial_chunks) {
                    Js5InputStream stream = new Js5InputStream(read().array());
                    for (int groupIndex = 0; stream.available() >= 6; ++groupIndex) {
                        int size = stream.readBytes(3);
                        int index = stream.readBytes(3);
                        //System.out.println("groupIndex: "+groupIndex+" size: "+size+" index: "+index);
                        initial_chunks.put(
                            groupIndex,
                            new InitialChunkInfo(groupIndex, size, index)
                        );
                    }
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
                throw new IllegalStateException("Unable to initialize lookup table.");
            }
        }
        return initial_chunks;
    }

    boolean hasLoaded() {
        return getReferenceTable().initialized();
    }

    void load() {
        synchronized (referenceTable) {
            try {
                getReferenceTable().initialize(
                    js5_cache.getMetaIndexFile(),
                    js5_cache.getDat2File()
                );
            } catch (IOException e) {
                e.printStackTrace();
                throw new IllegalStateException("Unable to initialize reference table.");
            }
        }
    }

    public ReferenceTable getReferenceTable() {
        return referenceTable;
    }

    byte[] readChunk(InitialChunkInfo chunkInfo) throws IOException {
        return referenceTable.readChunk(js5_cache.getDat2File(), chunkInfo);
    }

    public int archiveIndex() {
        return archiveIndex;
    }
}
