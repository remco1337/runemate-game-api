package com.runemate.game.cache.io;

import com.runemate.game.cache.util.*;
import java.io.*;
import java.nio.*;

public class Js5InputStream extends DataInputStream {
    private static final char[] CP1252_REPLACEMENT_TABLE =
        {
            '€', ' ', '‚', 'ƒ', '„', '…', '†', '‡', 'ˆ', '‰', 'Š', '‹',
            'Œ', ' ', 'Ž', ' ', ' ', '‘', '’', '“', '”', '•', '–', '—', '˜', '™', 'š', '›', 'œ',
            ' ', 'ž', 'Ÿ'
        };

    public Js5InputStream(byte[] data) {
        super(new OpenByteArrayInputStream(data));
    }

    /**
        @deprecated for internal use only
     */
    @SuppressWarnings("DeprecatedIsStillUsed")
    @Deprecated
    public static char getCP1252ReplacementAt(int index) {
        return CP1252_REPLACEMENT_TABLE[index];
    }

    public void decipherXTEA(int[] keys, int lengthOfEncrypted) throws IOException {
        int initialPosition = position();
        int numRounds = lengthOfEncrypted >> 3;
        for (int j1 = 0; j1 < numRounds; j1++) {
            int sum = 0x9E3779B9 * 32;
            int v0 = readInt();
            int v1 = readInt();
            for (int j = 0; j < 32; j++) {
                v1 -= (((v0 << 4) ^ (v0 >>> 5)) + v0) ^ (sum + keys[(sum >>> 11) & 3]);
                sum -= 0x9E3779B9;
                v0 -= (((v1 << 4) ^ (v1 >>> 5)) + v1) ^ (sum + keys[sum & 3]);
            }
            seek(position() - 8);
            writeInt(v0);
            writeInt(v1);
        }
        seek(initialPosition);
    }

    public byte[] getBackingArray() {
        return in().getBackingArray();
    }

    @Override
    public void mark(int readlimit) {
        throw new UnsupportedOperationException();
    }

    public int position() {
        return in().position();
    }

    public int readBytes(int amount) throws IOException {
        return readBytes(amount, 0);
    }

    public int readBytes(int amount, int offset) throws IOException {
        if (amount < 0) {
            throw new BufferUnderflowException();
        }
        if (amount > 4) {
            throw new IllegalArgumentException("Only 4 bytes can be read into an int.");
        }
        byte[] bytes = new byte[amount];
        int read = read(bytes);
        if (read == -1) {
            throw new EOFException();
        }
        return Bytes.join(bytes, offset);
    }

    /**
     * Reads an unsigned smart that can return -1, to indicate a default or "null" value.
     *
     * @throws IOException
     */
    public int readDefaultableUnsignedSmart() throws IOException {
        if (in().peek() < 0) {
            //Clears the sign bit (most significant bit), forcing the value to be positive.
            //The possible values are between 0 and Integer.MAX_VALUE
            return readInt() & Integer.MAX_VALUE;
        }
        int value = readUnsignedShort();
        if (value == Short.MAX_VALUE) {
            return -1;
        }
        return value;
    }

    public final String readJstr() throws IOException {
        if (readUnsignedByte() != 0) {
            throw new IOException("Invalid header for jstr");
        }
        return readCStyleLatin1String();
    }

    public final String readFailFastCString() throws IOException {
        if (readUnsignedByte() == 0) {
            return null;
        }
        return readCStyleLatin1String();
    }

    /**
     * Reads a compressed int from the stream with either a value between
     * -64 and 191 or -49152 and 16383 (Short.MAX_VALUE / 2)
     *
     * @throws IOException
     */
    public int readQuarterBoundSmart() throws IOException {
        if (Byte.toUnsignedInt(in().peek()) <= Byte.MAX_VALUE) {
            //Possible values are between -64 and 191
            return readUnsignedByte() - (Byte.MAX_VALUE / 2) - 1;
        }
        //Possible values are between -49152 and 16383 (Short.MAX_VALUE /2)
        return readUnsignedShort() - (int) ((double) (Short.MAX_VALUE - Short.MIN_VALUE) * 3 / 4) -
            1;
    }

    /**
     * Reads a compressed int from the stream with either a value between
     * 0 and Byte.MAX_VALUE (127) or Short.MIN_VALUE (-32768) and Short.MAX_VALUE (32767)
     *
     * @throws IOException
     */
    public int readReducedShortSmart() throws IOException {
        if (Byte.toUnsignedInt(in().peek()) <= Byte.MAX_VALUE) {
            return readUnsignedByte();
        }
        //Unsigned shorts range from 0 to 65535
        //Signed shorts have a max value of 32767
        //Some possible results:
        //unsigned = 0, so 0 - 32767 - 1 = -32768, aka Short.MIN_VALUE
        //unsigned = 65535, so 65535 - 32767 - 1 = 32767, aka Short.MAX_VALUE
        //So this converts it back to a signed value... is this right?
        return readUnsignedShort() - Short.MAX_VALUE - 1;
    }

    public int readShortSmart() throws IOException {
        int value = in().peek() & 0xFF;
        return value < 128 ? this.readUnsignedByte() : this.readUnsignedShort() - 0x8000;
    }

    public int readUnsignedIntSmartShortCompat() throws IOException {
        int value = 0;
        int incremented;
        for (incremented = readShortSmart(); incremented == Short.MAX_VALUE;
             incremented = this.readShortSmart()) {
            value += Short.MAX_VALUE;
        }
        return value + incremented;
    }

    public final String readCStyleLatin1String() throws IOException {
        StringBuilder builder = new StringBuilder();
        int character;
        while ((character = readUnsignedByte()) != 0) {
            if (character >= 0x80 && character < 0xa0) {
                char value = getCP1252ReplacementAt(character - 0x80);
                builder.append(value != 0 ? value : '?');
            } else {
                builder.append((char) character);
            }
        }
        return builder.toString();
    }

    /**
     * Reads a compressed int from the stream with either a value between
     * 0 and Integer.MAX_VALUE (2147483647) or 0 and UnsignedShort.MAX_VALUE (65535)
     *
     * @throws IOException
     */
    public int readUnsignedSmart() throws IOException {
        if (in().peek() < 0) {
            //Clears the sign bit (most significant bit), forcing the value to be positive.
            //The possible values are between 0 and Integer.MAX_VALUE
            return readInt() & Integer.MAX_VALUE;
        }
        return readUnsignedShort();
    }


    public int readVarInt2() throws IOException {
        int value = 0;
        int bits = 0;
        int read;
        do
        {
            read = readUnsignedByte();
            value |= (read & 0x7F) << bits;
            bits += 7;
        } while (read > 127);
        return value;
    }

    public void seek(int position) {
        in().position(position);
    }

    public int size() {
        return in().size();
    }

    public void write(int i) throws IOException {
        write(i, ByteOrder.nativeOrder());
    }

    public void write(int i, ByteOrder order) throws IOException {
        write(ByteBuffer.allocate(4).order(order).putInt(i).array());
    }

    public void write(byte[] bytes) throws IOException {
        if (bytes == null) {
            throw new IllegalArgumentException();
        }
        for (byte b : bytes) {
            write(b);
        }
    }

    private void write(byte b) throws IOException {
        in().getBackingArray()[in().position()] = b;
        skipBytes(1);
    }

    private OpenByteArrayInputStream in() {
        return (OpenByteArrayInputStream) in;
    }


    private void writeByte(final byte b) throws IOException {
        write(b);
    }

    private void writeInt(int i) throws IOException {
        writeByte((byte) (i >> 24));
        writeByte((byte) (i >> 16));
        writeByte((byte) (i >> 8));
        writeByte((byte) i);
    }
}
