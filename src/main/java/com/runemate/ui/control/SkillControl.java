package com.runemate.ui.control;

import com.runemate.game.internal.*;
import com.runemate.ui.*;
import com.runemate.ui.binding.*;
import com.runemate.ui.tracker.*;
import java.net.*;
import java.util.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;
import lombok.*;

@InternalAPI
public class SkillControl extends GridPane implements Initializable {

    @FXML
    private ProgressBar progressBar;

    @FXML
    private Text progressText, skillText, ttl, xpGained, xpLeft, xpRate, currentLevel, nextLevel;

    private final DefaultUI ui;
    private final SkillsTracker.SkillTracker tracker;

    public SkillControl(@NonNull DefaultUI ui, @NonNull SkillsTracker.SkillTracker tracker) {
        this.ui = ui;
        this.tracker = tracker;
        FXUtil.loadFxml(this, "/fxml/skill_tracker.fxml");
        FXUtil.loadCss(this, "/css/skill_tracker.css");
        FXUtil.loadCss(this, "/resources/css/categories.css");
    }

    @Override
    public void initialize(final URL url, final ResourceBundle resourceBundle) {
        progressBar.progressProperty().bind(tracker.getProgress());
        progressBar.getStyleClass().add("category-" + tracker.getSkill().name().toLowerCase());

        progressText.textProperty().bind(progressBar.progressProperty().multiply(100).asString("%.1f%%"));
        skillText.setText(tracker.getSkill().toString());
        xpGained.textProperty().bind(new MetricBinding(tracker.getExperienceGained()));
        xpLeft.textProperty().bind(new MetricBinding(tracker.getExperienceLeft()));

        final var rate = new RateBinding(tracker.getExperienceGained(), ui.runtimeProperty());
        xpRate.textProperty().bind(new MetricBinding(rate));
        ttl.textProperty().bind(new TimestampBinding(tracker.getExperienceLeft().divide(rate.divide(360_000))));

        currentLevel.textProperty().bind(tracker.getLevel().asString("Lvl: %s"));
        nextLevel.textProperty().bind(tracker.getLevel().add(1).asString("Lvl: %s"));
    }
}
