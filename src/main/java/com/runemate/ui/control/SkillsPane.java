package com.runemate.ui.control;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.internal.*;
import com.runemate.ui.*;
import com.runemate.ui.tracker.*;
import javafx.application.*;
import javafx.collections.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.*;
import lombok.experimental.*;

@InternalAPI
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SkillsPane extends TitledPane {

    DefaultUI ui;
    FlowPane container;

    public SkillsPane(@NonNull DefaultUI ui) {
        this.ui = ui;
        final var tracker = new SkillsTracker(ui.bot());
        tracker.getSkills().addListener((MapChangeListener<Skill, SkillsTracker.SkillTracker>) change -> {
            if (change.wasAdded()) {
                trackSkill(change.getValueAdded());
            }
        });

        container = new FlowPane();
        container.setHgap(12);
        container.setVgap(12);
        container.setPadding(new Insets(12));

        setText("Skills");
        setContent(container);
        setExpanded(false);
    }

    private void trackSkill(@NonNull SkillsTracker.SkillTracker tracker) {
        Platform.runLater(() -> {
            ui.bot().getLogger().info("[SkillTracker] Adding tracker for " + tracker.getSkill().name());
            container.getChildren().add(new SkillControl(ui, tracker));
        });
    }
}
